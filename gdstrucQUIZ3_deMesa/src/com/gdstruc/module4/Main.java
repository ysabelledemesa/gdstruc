package com.gdstruc.module4;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	ArrayQueue queue = new ArrayQueue(5);

	for (int i = 0; i < 10; i++)
	{
		int numberPlayers = randomNumber(1, 7);

		int count = 0;

		for (int j = 0; j < numberPlayers; j++)
		{
			count++;
			queue.add(new Player("A Player Has Joined"));
		}

		queue.printQueue();
		enterPrompt();

		if (count >= 5)
		{
			for (int k = 0; k < 5; k++)
			{
				queue.remove();
			}
		}
	}

    }

    public static int randomNumber(int min, int max)
	{
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	public static void enterPrompt()
	{
		System.out.println("Press ENTER to continue");
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
	}
}
