package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        LinkedStack stack = new LinkedStack();

        stack.push(new Player(1, "aceu", 100));
        stack.push(new Player( 2, "Sinatraa", 100));
        stack.push(new Player (3, "Subroza", 95));
        //last element is the one on top

        stack.printStack();

        //removing last element
        System.out.println("Popping: " + stack.pop());
        stack.printStack();

        //peeking
        System.out.println("Peeking: " + stack.peek());
    }
}
