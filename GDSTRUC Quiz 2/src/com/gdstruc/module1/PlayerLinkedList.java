package com.gdstruc.module1;

public class PlayerLinkedList {
    //reference to the head
    private PlayerNode head;

    //add element in front of head
    public void addToFront(Player player)
    {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    //how to know if at the end (NULL)
    public void printList() {
        PlayerNode current = head;
        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("NULL");
    }

    public void removeFirst() {
        PlayerNode current = head;
        current = null;
        current = current.getNextPlayer();
    }

    public void sizeVariable() {
        int size = 0;
        PlayerNode current = head;
        while (current != null) {
            size++;
            System.out.print(size);
        }
    }

    public boolean indexOf(Player player) {
        indexOf(player);
    }

    public boolean contains(Player player) {
        contains(player);
    }
}
