package com.gdstruc.module1;

import java.util.EmptyStackException;

public class ArrayStack {
    private Player[] stack;
    private int top;

    public ArrayStack(int capacity)
    {
        stack = new Player[capacity];
    }

    public void push(Player player)
    {
        if (top == stack.length) //if stack is full
        {
            //resize
            Player [] newStack = new Player[2 * stack.length];
            //copy contents
            System.arraycopy(stack, 0, newStack, 0, stack.length);
            stack = newStack;
        }

        //actual function of push
        stack[top++] = player;
    }

    public Player pop()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }

        Player poppedPlayer = stack[--top];
        stack[top] = null;
        return poppedPlayer;
    }

    public Player peek()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }

        return stack[top - 1];
    }

    public boolean isEmpty()
    {
        return top == 0;
    }

    public void printStack()
    {
        System.out.println("Printing stack:");

        for (int i = top - 1; i >= 0; i--) //printing from top to bottom
        {
            System.out.println(stack[i]);
        }
    }
}
