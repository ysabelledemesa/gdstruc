package com.gdstruc.module4;

import java.util.NoSuchElementException;

public class ArrayQueue {

    private Player[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity)
    {
        queue = new Player[capacity];
    }

    //back = length - 1
    public void add(Player player)
    {
        //check if queue is full
        if (back == queue.length)
        {
            //add space
            Player[] newQueue = new Player[queue.length * 2];
            //copying content of old array to the new one
            System.arraycopy(queue, 0, newQueue, 0, queue.length);
            queue = newQueue;
        }

        //adding the new element
        queue[back] = player;
        back++;
    }

    public Player remove()
    {
        //check if queue is empty
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        //removing element
        Player removedPlayer = queue[front];
        queue[front] = null;
        front++;

        //check if queue is now empty
        if (size() == 0)
        {
            //reset front and back
            front = 0;
            back = 0;
        }

        return removedPlayer;
    }

    public Player peek() //accessing front element
    {
        if (size() == 0)
        {
            throw new NoSuchElementException();
        }

        return queue[front];
    }

    //size of queue
    public int size()
    {
        return back - front;
    }

    public void printQueue()
    {
        for (int i = front; i < back; i++)
        {
            System.out.println(queue[i]);
        }
    }
}
