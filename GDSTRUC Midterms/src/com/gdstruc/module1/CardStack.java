package com.gdstruc.module1;

import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Random;

public class CardStack {
    private LinkedList<Card> stack;
    private int top;

    public CardStack()
    {
        stack = new LinkedList<Card>();
    }

    public void push (Card card)
    {
        stack.push(card);
    }

    public Card pop()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }

        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public boolean isEmpty()
    {
        return stack.isEmpty();
    }

    public void printHand()
    {
        ListIterator<Card> iterator = stack.listIterator();
        System.out.println("You have " + stack.toArray().length + " cards in your hand");
        System.out.println("These are your cards: ");
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public void printDeck()
    {
        System.out.println("You have " + stack.toArray().length + " cards in the player's deck");
    }

    public void printDiscard()
    {
        System.out.println("You have " + stack.toArray().length + " cards in the discard pile");
    }
}
