package com.gdstruc.module5;

public class SimpleHashTable {
    private StoredPlayer[] hashtable;

    public SimpleHashTable()
    {
        hashtable = new StoredPlayer[10];
    }

    //ensuring the the key is mapped in the hashtable range
    private int hashKey(String key)
    {
        //determines key with number of characters in string
        return key.length() % hashtable.length;
    }

    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);

        //check if key is occupied
        if (isOccupied(hashedKey))
        {
            //starts linear probing when collision is detected
            //stops probing once original/colliding key is reached
            int stoppingIndex = hashedKey;

            //wrap around once last key is reached
            if (hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }

            else
            {
                hashedKey++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                //ensures key is still within range
                hashedKey = (hashedKey + 1) % hashtable.length;
            }
        }

        //check if key is occupied, avoid collision
        if (isOccupied(hashedKey))
        {
            System.out.println("There is already a value at position " + hashedKey);
        }

        else
        {
            hashtable[hashedKey] = new StoredPlayer(key, value);
        }
    }

    public Player get(String key)
    {
        int hashedKey = findKey(key);

        if (hashedKey == -1)
        {
            return null;
        }
        return hashtable[hashedKey].value;
    }

    public Player remove(String key)
    {
        int hashedKey = findKey(key);

        if (hashtable[hashedKey] != null)
        {
            hashtable[hashedKey] = null;
        }
        return null;
    }

    private int findKey (String key)
    {
        int hashedKey = hashKey(key);

        //check if key is same as key in linear probing
        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

            //starts linear probing when collision is detected
            //stops probing once original/colliding key is reached
        int stoppingIndex = hashedKey;

        //wrap around once last key is reached
        if (hashedKey == hashtable.length - 1) {
            hashedKey = 0;
        } else {
            hashedKey++;
        }

        while (hashedKey != stoppingIndex && hashtable[hashedKey] != null && !hashtable[hashedKey].key.equals(key)) {
            //ensures key is still within range
            hashedKey = (hashedKey + 1) % hashtable.length;
        }

        if (hashtable[hashedKey] != null && hashtable[hashedKey].key.equals(key))
        {
            return hashedKey;
        }

        return -1;
    }

    private boolean isOccupied(int index)
    {
        //checks if key is occupied
        return hashtable[index] != null;
    }

    public void printHashTable()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            System.out.println("Element " + i + " " + hashtable[i]);
        }
    }

}
